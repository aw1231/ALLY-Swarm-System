import cgi
from google.appengine.ext import webapp
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext.webapp.util import run_wsgi_app

class Userstate(db.Model):
    googleuser = db.UserProperty()
    input = db.StringProperty(multiline=True)
    username = db.StringProperty(multiline=False)
class MainPage(webapp.RequestHandler):
    def get(self):
        user = users.get_current_user()

        if user:
            self.response.out.write(""" <html> <body> Hello, """ + user.nickname() + '. Welcome to the ALLY Swarm System. ')
            self.response.out.write("""<form action="/response" method="post"> <br /> To begin, please enter your input here: <input type="text" name="userinput" /> <br /> <input type="submit" value="Tell ALLY">""")
            self.response.out.write("""</form> For more information look <a href="/about">here</a>  </body> </html>""")
        else:
            self.redirect(users.create_login_url(self.request.uri))

class Response(webapp.RequestHandler):
    def post(self):
        self.response.out.write('<html><body>You wrote:<pre>')
        self.response.out.write(cgi.escape(self.request.get('userinput')))
        self.response.out.write('</pre></body></html>')

class About(webapp.RequestHandler):
    def get(self):
        self.response.out.write('<html><body><script type="text/javascript" src="http://www.ohloh.net/p/589384/widgets/project_cocomo.js"></script></body></html>')

application = webapp.WSGIApplication(
                                     [('/', MainPage),
                                      ('/response', Response),
                                      ('/about', About)],
                                     debug=True)

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()
